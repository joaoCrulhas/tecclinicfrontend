import lazyLoading from './lazyLoading'

export default {
  name: 'Patients',
  meta: {
    expanded: false,
    title: 'patient.patient',
    iconClass: 'fa fa-address-book'
  },
  children: [
    {
      name: 'AddPatients',
      path: '/addpatients',
      component: lazyLoading('patients/PatientsAdd'),
      meta: {
        title: 'patient.add'
      }
    },
    {
      name: 'ListPatients',
      path: '/listpatients',
      component: lazyLoading('patients/PatientsList'),
      meta: {
        title: 'patient.list'
      }
    }
  ]
}

